/*yqy 2016.1*/
/*字符串处理函数part1 库函数
由于2015年1月的考题出现了“不允许使用字符串库函数”的坑爹话，只好自己编咯orz*/
/*收录了
1、strncpy,strncat,strncmp(作业题)
2、strlen,strcmp,strcpy(自己+百度)*/ 
#include<stdio.h>
char *strncpy(char *s, char *t, int n)
{
	int i;
	int len = 0;
	while(*(s+len) != '\0') //先找字符串s的长度
		len++;
	for(i = 0; i < n; i++) //依次将字符串t的前n个字符拷贝到字符串s
		*(s+i) = *(t+i);
	if(n > len) //如果拷贝的长度超过了s的长度，则添加结尾符
		*(s+i) = '\0';
	return s;
}
char *strncat(char *s, char *t, int n)
{
	int i = 0, len = 0;
	while(*(s+len) != '\0') //先找字符串s的长度
		len++;
	for(i = 0; i < n; i++) //然后将t的前n个字符依次拷贝到字符串s后面
		*(s+len+i) = *(t+i);
	*(s+len+i) = '\0'; //添加结尾符
	return s;
}
int strncmp(char *s, char *t, int n)
{
	int i = 0;
	while(*(s+i) == *(t+i) && i < n) //逐个比较s和t中的字符，直到遇到第一个不相等的情况
		i++;
	if(i == n) //如果比较完n个字符之后都相等，返回0
		return 0;
	else if(*(s+i) < *(t+i)) //否则根据不相等的字符进行判断
		return -1;
	else
		return 1;
}
int strlen(const char *s)//自己编的，没查出什么问题 
{
	int i;
	for(i=0;s[i]!='\0';i++)
		;
	return i;
}
int __cdecl strcmp(const char *src,const char *dst)//百度的，经测试可以 
{    
	int ret=0;    
	while(!(ret = *(unsigned char *)src - *(unsigned char *)dst) && *dst)
	    ++src,++dst;    
	if(ret<0)        
		ret=-1;    
	else if(ret>0)        
		ret=1;    
	return(ret);
}
char * strcpy(char * strDest,const char * strSrc)//一看就是百度的 
{
	if ((NULL==strDest) || (NULL==strSrc)) //[1]
	throw "Invalid argument(s)"; //[2]
	char * strDestCopy = strDest; //[3]
	while ((*strDest++=*strSrc++)!='\0'); //[4]
	return strDestCopy;
}
int main()
{
	char s[20],test1[]="abc",q[10],test2[]="bc";
	char t[20];
	int n;
	/*while(1)
	{
		scanf("%s",q);
		printf("%d\n",strcmp(q,"exit"));
	}*/
	//strcpy(q,"hi");
	//printf("%s",q); 
	printf("%d",strlen(test1));
	//gets(s);
	//fflush(stdin);
	//gets(t);
	//scanf("%d",&n);
	//strncpy(s, t, n);
	//printf("%s\n", s);
	//strncat(s, t, n);
	//printf("%s\n", s);
	//printf("%d\n", strncmp(s, t, n));
} 
