addpath(genpath(pwd));
name = '001L';
name_string = strcat(name,'.stl');
[v,f,n,a] = stlRead(name_string);
stlPlot(v,f,a);

%右手的测点标记顺序：（1）手背中心点；（2）靠小指侧边点；（3）靠大拇指侧边点；（4）尺骨茎突点。
%左手的测点标记顺序：（1）手背中心点；（2）靠大拇指侧边点；（3）靠小指侧边点；（4）尺骨茎突点。
