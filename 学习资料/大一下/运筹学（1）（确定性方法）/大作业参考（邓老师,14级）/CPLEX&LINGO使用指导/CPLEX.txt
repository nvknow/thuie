模型文件
字符串（定义产品）
{string}Prduct={"table","chair"};

数值：
1）浮点数（变量名、对应字符串数组变量名、数值）
float Profit[Prcoduct]=[50,30];
2）整数
int 
3）连续恒数据区间
range Rows=1...10;
4）决策变量
davr float+Production[Product]
5)目标函数
6）约束
subject to{约束1：}
7）表达式符号 ==，<=,!=,<

数据文件（如果在模型文件中都描述清楚就不用了，如果没有的话还需要一个数据文件重新定义）
字符串：变量名={字符串1，……}
数值：	顺序不可调：数组变量名称=[数值1，数值2……]
	顺序可调整：数组变量名称=#[变量名1：数值1 变量名2：数值2……]

对求解进行调整：Set.ops




/*********************************************
 * OPL 12.6.3.0 Model
 * Author: zhupeihao
 * Creation Date: 2016-4-6 at 上午10:16:50
 *********************************************
{string} Product={"table","chair"};
{string} Worktype={"wood","painter"};
float Profit[Prcoduct]=[50,30];
float Time[Product][worktype]=[[4,2],[3,1]];
float Total[Worktype]=[120,50];
dvar float Prodcution[Product];

maximize 
	50*Prodcution["table"]+30*Production["chair"];
subject to{
forall(i in worktype)
	sum(p in Product)Time[p][i]*Porduction[p]<=Total[i];
}*/
{string} Products =  {"gas","chloride"};
dvar float production[Products]; 
maximize 
      40 * production["gas"] + 50 * production["chloride"]; 
subject to {
      production["gas"] + production["chloride"] <= 50; 
      3 * production["gas"] + 4 * production["chloride"] <= 180; 
      production["chloride"] <= 40; 
}
