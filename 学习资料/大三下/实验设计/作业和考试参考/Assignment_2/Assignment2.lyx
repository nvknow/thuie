#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass article
\begin_preamble
% 如果没有这一句命令，XeTeX会出错，原因参见
% http://bbs.ctex.org/viewthread.php?tid=60547
\DeclareRobustCommand\nobreakspace{\leavevmode\nobreak\ }
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package none
\inputencoding auto
\fontencoding global
\font_roman newcent
\font_sans helvet
\font_typewriter courier
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format pdf4
\output_sync 0
\bibtex_command default
\index_command default
\float_placement H
\paperfontsize default
\spacing single
\use_hyperref false
\papersize a4paper
\use_geometry true
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 2.5cm
\topmargin 3cm
\rightmargin 2.5cm
\bottommargin 3cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Assignment 2 of DOE
\end_layout

\begin_layout Author
LIU Zhe, 2009010845
\end_layout

\begin_layout Date
Mar 21, 2012
\end_layout

\begin_layout Section
Montgomery 2-5
\end_layout

\begin_layout Subsection*
Solution:
\end_layout

\begin_layout Description
(a) 
\begin_inset Formula 
\[
H_{0}:\mu\le120
\]

\end_inset


\begin_inset Formula 
\[
H_{1}:\mu>120
\]

\end_inset


\end_layout

\begin_layout Description
(b) From the data we could obtain
\begin_inset Formula 
\[
n=10,\,\bar{x}=131,\, s^{2}=382
\]

\end_inset

So the value of the test statistic is
\begin_inset Formula 
\[
t=\frac{\bar{x}-\mu}{s/\sqrt{n}}=1.78
\]

\end_inset

It could be found that for 
\begin_inset Formula $\alpha=0.01$
\end_inset

, 
\begin_inset Formula $t_{0.01,9}=2.821$
\end_inset

.
 Thus 
\begin_inset Formula $H_{0}$
\end_inset

 can not be rejected, meaning we cannot demonstrate that the mean shelf
 life exceeds 120.
\end_layout

\begin_layout Description
(c) From (b) we can get the p-value as
\begin_inset Formula 
\[
p=P(t\ge1.78)=0.054
\]

\end_inset


\end_layout

\begin_layout Description
(d) Since
\begin_inset Formula 
\[
\bar{x}-t_{0.01/2,9}\frac{s}{\sqrt{n}}=110.91,\,\,\bar{x}+t_{0.01/2,9}\frac{s}{\sqrt{n}}=151.09
\]

\end_inset

The 99% confidence interval on the mean shelf life is
\begin_inset Formula 
\[
(110.91,151.09)
\]

\end_inset


\end_layout

\begin_layout Section
3D Fusion Time
\end_layout

\begin_layout Subsection*
Solution:
\end_layout

\begin_layout Description
(1) First we compute the descriptive statistics and draw histogram and box-plot
 of the data using Minitab.
\begin_inset Newline newline
\end_inset


\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename Pictures/Descriptive Statistics.png
	scale 70

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Descriptive Statistics
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename Pictures/Q2_Histogram.png
	scale 45

\end_inset

 
\begin_inset Graphics
	filename Pictures/Q2_boxplot.png
	scale 45

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Histogram and Box-plot
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset

From the result above, we find VV group has a smaller mean value than NV
 group, indicating the members in VV group need less time to fuse the image.
 Moreover, both group is positive skewed and NV has relatively larger variabilit
y, and there is one very large potential outlier in NV group.
\end_layout

\begin_layout Description
(b) Since we want to test whether knowledge of the form of the embedded
 image affected the time required for subjects to fuse the images, we propose
 the following hypotheses:
\begin_inset Formula 
\[
H_{0}:\mu_{1}-\mu_{2}=0
\]

\end_inset


\begin_inset Formula 
\[
H_{1}:\mu_{1}-\mu_{2}\ne0
\]

\end_inset

where 
\begin_inset Formula $\mu_{1}$
\end_inset

 and 
\begin_inset Formula $\mu_{2}$
\end_inset

 are the mean time of NV and VV group, respectively.
\begin_inset Newline newline
\end_inset

Using Minitab, we conduct statistical tests of the hypotheses and get the
 following results.
\end_layout

\begin_layout Itemize
Equal-variance T-test:
\end_layout

\begin_layout LyX-Code
Difference = mu (NV) - mu (VV) 
\end_layout

\begin_layout LyX-Code
Estimate for difference:  3.01 
\end_layout

\begin_layout LyX-Code
95% CI for difference:  (-0.08, 6.10) 
\end_layout

\begin_layout LyX-Code
T-Test of difference = 0 (vs not =): T-Value = 1.94  P-Value = 0.056  DF =
 76 
\end_layout

\begin_layout LyX-Code
Both use Pooled StDev = 6.8149
\end_layout

\begin_layout Itemize
Unequal-variance T-test:
\end_layout

\begin_layout LyX-Code
Difference = mu (NV) - mu (VV) 
\end_layout

\begin_layout LyX-Code
Estimate for difference:  3.01 
\end_layout

\begin_layout LyX-Code
95% CI for difference:  (0.06, 5.95) 
\end_layout

\begin_layout LyX-Code
T-Test of difference = 0 (vs not =): T-Value = 2.04  P-Value = 0.045  DF =
 70
\end_layout

\begin_layout Standard
Based on the p-values of the two t-tests above, we can conclude that at
 the 0.05 significance level, no difference between
\begin_inset Formula $\mu_{1}$
\end_inset

 and 
\begin_inset Formula $\mu_{2}$
\end_inset

 are demonstrated if we use equal-variance.
 However, analysis these data using unequal-variance reveals difference
 between the groups.
\end_layout

\begin_layout Standard
However, in using the t-test procedure we make the assumption that both
 samples are drawn from independent populations that can be described by
 
\emph on
normal distributions
\emph default
, the histogram and box-plot in (a) do not seem to agree with this assumption
 because the data are skewed and have unequal variance.
\end_layout

\begin_layout Standard
Therefore, we should check the normality of the data.
 The result is shown below.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename Pictures/Q2_probability.png
	scale 55

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Probability-plot
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
From the figure above, the data of the two groups are not normal at 95%
 confidence level.
 And there is a very large outlier in NV group.
 So we cancel out the outlier in NV group and then use Box-Cox transformation
 to transform the data to normal distribution.
 The figure below shows the result of Box-Cox transformation.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename Pictures/Q2_boxcoxNV.png
	scale 50

\end_inset

 
\begin_inset Graphics
	filename Pictures/Q2_boxcoxVV.png
	scale 50

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Box-Cox Transformation
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
From the above result, both group could be transformed to normal distribution
 by taking logarithm.
 The figure below illustrates the descriptive statistics, histograms, boxplots
 and probability-plot of the log-value of the two groups (Outlier has been
 canceled out in the various plots).
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename Pictures/Descriptive Statistics_log.png
	scale 70

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Descriptive Statistics after log transformation
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename Pictures/Q2_histogram_log.png
	scale 45

\end_inset

 
\begin_inset Graphics
	filename Pictures/Q2_boxplot_log.png
	scale 45

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Histogram and Box-plot after log transformation
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename Pictures/Q2_probability_log.png
	scale 55

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Probability-plot after log transformation
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Before making further t-test, we first make a F-test to see whether the
 variances of the two groups are equal.
 The hypotheses are:
\begin_inset Formula 
\[
H_{0}:\sigma_{1}=\sigma_{2}
\]

\end_inset


\begin_inset Formula 
\[
H_{1}:\sigma_{1}\ne\sigma_{2}
\]

\end_inset

The testing result is:
\end_layout

\begin_layout LyX-Code

\emph on
Statistics
\end_layout

\begin_layout LyX-Code
Variable   N  StDev  Variance 
\end_layout

\begin_layout LyX-Code
log-NV    42  0.758     0.575 
\end_layout

\begin_layout LyX-Code
log-VV    35  0.818     0.669
\end_layout

\begin_layout LyX-Code
Ratio of standard deviations = 0.927 
\end_layout

\begin_layout LyX-Code
Ratio of variances = 0.860
\end_layout

\begin_layout LyX-Code

\end_layout

\begin_layout LyX-Code

\emph on
Tests
\end_layout

\begin_layout LyX-Code
                                               Test 
\end_layout

\begin_layout LyX-Code
Method
\emph on
 
\emph default
                         DF1  DF2  Statistic  P-Value 
\end_layout

\begin_layout LyX-Code
F Test (normal)                  41   34       0.86    0.639 
\end_layout

\begin_layout LyX-Code
Levene's Test (any continuous)    1   75       0.08    0.774
\end_layout

\begin_layout Standard
It is clear that we cannot reject the null hypothesis due to the p-value
 (0.639).
\end_layout

\begin_layout Standard
So now we could conduct t-test again on the log data using equal-variance.
 The result is shown below.
\end_layout

\begin_layout LyX-Code

\emph on
Two-Sample T-Test and CI: log-fusionTime, Group:
\end_layout

\begin_layout LyX-Code
Difference = mu (NV) - mu (VV) 
\end_layout

\begin_layout LyX-Code
Estimate for difference:  0.431 
\end_layout

\begin_layout LyX-Code
95% CI for difference:  (0.061, 0.800) 
\end_layout

\begin_layout LyX-Code
T-Test of difference = 0 (vs not =): T-Value = 2.32  P-Value = 0.023  DF =
 76 
\end_layout

\begin_layout LyX-Code
Both use Pooled StDev = 0.8156
\end_layout

\begin_layout Standard
This time the p-value is 0.023, showing a clearly significant difference
 in means.
\end_layout

\begin_layout Description
(c) Based on the above analysis, there is significant difference in log-means
 of the two groups at 95% confidence level, which indicates that the knowledge
 of the form of the embedded image does affect the time required to fuse
 the images, and the VV group needs less time than the NV group.
\end_layout

\begin_layout Section
Sewage Experiment
\end_layout

\begin_layout Subsection*
Solution:
\end_layout

\begin_layout Standard
The descriptive statistics of the data is shown below.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename Pictures/Q3_descriptiveStatistics.png
	scale 80

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Descriptive statistics
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
We first make an F-test to check the equality of the variance of the two
 groups, the result is as follows.
\end_layout

\begin_layout LyX-Code

\emph on
Statistics
\end_layout

\begin_layout LyX-Code
Variable  N  StDev  Variance 
\end_layout

\begin_layout LyX-Code
MSI       8  4.220    17.811 
\end_layout

\begin_layout LyX-Code
SIB       8  4.125    17.013
\end_layout

\begin_layout LyX-Code
Ratio of standard deviations = 1.023 Ratio of variances = 1.047
\end_layout

\begin_layout LyX-Code

\end_layout

\begin_layout LyX-Code

\emph on
Tests
\end_layout

\begin_layout LyX-Code
                                               Test 
\end_layout

\begin_layout LyX-Code
Method                          DF1  DF2  Statistic  P-Value 
\end_layout

\begin_layout LyX-Code
F Test (normal)                   7    7       1.05    0.953 
\end_layout

\begin_layout LyX-Code
Levene's Test (any continuous)    1   14       0.00    0.966
\end_layout

\begin_layout Standard
It is confirmed by the big p-value that we cannot reject the null hypothesis.
 So for the following test we could assume equal variance.
\end_layout

\begin_layout Standard
Now we conduct an unpaired t-test assuming equal variance, the result is
 as follows.
\end_layout

\begin_layout LyX-Code

\emph on
Two-sample T for MSI vs SIB:
\end_layout

\begin_layout LyX-Code
Difference = mu (MSI) - mu (SIB) 
\end_layout

\begin_layout LyX-Code
Estimate for difference:  -0.41 
\end_layout

\begin_layout LyX-Code
95% CI for difference:  (-4.89, 4.06) 
\end_layout

\begin_layout LyX-Code
T-Test of difference = 0 (vs not =): T-Value = -0.20  P-Value = 0.846  DF
 = 14 
\end_layout

\begin_layout LyX-Code
Both use Pooled StDev = 4.1727
\end_layout

\begin_layout Standard
From the p-value we know that the unpaired t-test fails to declare significant
 difference.
\end_layout

\begin_layout Standard
Then we conduct a paired t-test, and shown below is the corresponding result.
\end_layout

\begin_layout LyX-Code

\emph on
Paired T for MSI - SIB:
\end_layout

\begin_layout LyX-Code
            N    Mean  StDev  SE Mean 
\end_layout

\begin_layout LyX-Code
MSI         8    5.02   4.22     1.49 
\end_layout

\begin_layout LyX-Code
SIB         8    5.43   4.12     1.46 
\end_layout

\begin_layout LyX-Code
Difference  8  -0.414  0.321    0.113
\end_layout

\begin_layout LyX-Code
95% CI for mean difference: (-0.682, -0.145) 
\end_layout

\begin_layout LyX-Code
T-Test of mean difference = 0 (vs not = 0): T-Value = -3.65  P-Value = 0.008
\end_layout

\begin_layout Standard
From the p-value of this test, significant difference of means in the two
 groups can be demonstrated under 95% confidence level, indicating that
 the two methods MSI and SIB are not equivalent.
\end_layout

\begin_layout Standard
It should be appropriate to use paired t-test because it is more effective,
 and is also due to the nature of the paired experiment.
\end_layout

\begin_layout Section
Cloud Seeding (Continued)
\end_layout

\begin_layout Subsection*
Solution:
\end_layout

\begin_layout Standard
Based on previous work in Assignment 1, we know that the log value of unseeded
 and seeded data are normally distributed at 95% confidence level, and the
 standard deviations of them are very close (1.642 vs 1.600).
 So we can make a two-sample t-test to check the equality of the mean of
 the two groups.
 But before that, we should first make an F-test to see whether we can assume
 equal variance.
 The result is as follows.
\end_layout

\begin_layout LyX-Code

\emph on
Statistics
\end_layout

\begin_layout LyX-Code
Variable       N  StDev  Variance 
\end_layout

\begin_layout LyX-Code
Log-Unseeded  26  1.642     2.696 
\end_layout

\begin_layout LyX-Code
Log-Seeded    26  1.600     2.558
\end_layout

\begin_layout LyX-Code
Ratio of standard deviations = 1.026 
\end_layout

\begin_layout LyX-Code
Ratio of variances = 1.054
\end_layout

\begin_layout LyX-Code

\end_layout

\begin_layout LyX-Code

\emph on
Tests
\end_layout

\begin_layout LyX-Code
                                               Test 
\end_layout

\begin_layout LyX-Code
Method                          DF1  DF2  Statistic  P-Value 
\end_layout

\begin_layout LyX-Code
F Test (normal)                  25   25       1.05    0.897 
\end_layout

\begin_layout LyX-Code
Levene's Test (any continuous)    1   50       0.08    0.781
\end_layout

\begin_layout Standard
The result above confirms our assumption of equal variance due to the big
 p-value.
 So we continued to make the t-test.
 The hypotheses are:
\begin_inset Formula 
\[
H_{0}:\mu_{1}-\mu_{2}\ge0
\]

\end_inset


\begin_inset Formula 
\[
H_{1}:\mu_{1}-\mu_{2}<0
\]

\end_inset


\end_layout

\begin_layout Standard
Using Minitab, we get the following result.
\end_layout

\begin_layout LyX-Code

\emph on
Two-sample T for Log-Unseeded vs Log-Seeded:
\end_layout

\begin_layout LyX-Code
               N  Mean  StDev  SE Mean 
\end_layout

\begin_layout LyX-Code
Log-Unseeded  26  3.99   1.64     0.32 
\end_layout

\begin_layout LyX-Code
Log-Seeded    26  5.13   1.60     0.31
\end_layout

\begin_layout LyX-Code
Difference = mu (Log-Unseeded) - mu (Log-Seeded) 
\end_layout

\begin_layout LyX-Code
Estimate for difference:  -1.144 
\end_layout

\begin_layout LyX-Code
95% upper bound for difference:  -0.390 
\end_layout

\begin_layout LyX-Code
T-Test of difference = 0 (vs <): T-Value = -2.54  P-Value = 0.007  DF = 50
 
\end_layout

\begin_layout LyX-Code
Both use Pooled StDev = 1.6208
\end_layout

\begin_layout Standard
The above result shows a p-value of -0.007, so at 5% significant level the
 null hypothesis should be rejected, indicating that the cloud seeding increases
 rainfall.
\end_layout

\end_body
\end_document
