/*
  Name: MilikanDataProcessing 
  Copyright: Liu Zhe
  Author: Liu Zhe
  Date: 13/11/10 15:09
  Description: 
*/
#include <iostream>
#include <iomanip>
using namespace std;

int checkMin(double data[], int num, double threshold)   //1 for min existed, 0 for not
{  
    for(int i=0; i < num; i++)
        if(data[i] >= threshold)
            return 1;
    return 0;
}

int findMin(double data[], int num, double threshold)    //Suppose min is existed
{
    int m(0);
    while(data[m]<threshold)
        m++;
    for(int i=m+1; i<num; i++)
        if(data[i]>=threshold && data[i]<=data[m])
            m = i;
    return m;
}

int main()
{
    cout << "***************************" << endl;
    cout << "  Milikan油滴实验数据处理  " << endl; 
    cout << "        工91  刘哲         " << endl; 
    cout << "***************************" << endl;
    int n, nk;  //Data amount & counting times
    cout << "请输入油滴数据个数：";
    cin >> n;
    nk = n/2;  //Counting n/2 times and evaluate the average
    double dat[n], dat1[n], dat2[n], st[nk]; //dat: original

    cout << "==========================" << endl;

    for(int i=0; i<n; i++){
        cout << "油滴" << i+1 << "的电量是(单位: 10^-19C):";
        cin >> dat[i];
        dat1[i] = dat[i]; 
    }

/*
    dat[0] = dat1[0] = 36.51;
    dat[1] = dat1[1] = 18.22;
    dat[2] = dat1[2] = 89.64;
    dat[3] = dat1[3] = 88.82;
    dat[4] = dat1[4] = 21.7;
    dat[5] = dat1[5] = 11.11;
*/
/*
    dat[0] = dat1[0] = 215.7;
    dat[1] = dat1[1] = 61.9;
    dat[2] = dat1[2] = 105.8;
    dat[3] = dat1[3] = 230.6;
    dat[4] = dat1[4] = 47.8;
    dat[5] = dat1[5] = 309.8;
*/
/*
    dat[0] = dat1[0] = 3.2378;
    dat[1] = dat1[1] = 4.88;
    dat[2] = dat1[2] = 3.2207;
    dat[3] = dat1[3] = 4.8057;
    dat[4] = dat1[4] = 3.1854;
    dat[5] = dat1[5] = 8.1209;
*/
/*
    dat[0] = dat1[0] = 2.92;
    dat[1] = dat1[1] = 4.62;
    dat[2] = dat1[2] = 4.76;
    dat[3] = dat1[3] = 4.83;
    dat[4] = dat1[4] = 6.21;
    dat[5] = dat1[5] = 6.41;
    dat[6] = dat1[6] = 7.82;
    dat[7] = dat1[7] = 8;
    dat[8] = dat1[8] = 9.36;
    dat[9] = dat1[9] = 9.51;
    dat[10] = dat1[10] = 14.13;
*/
/*
    dat[0] = dat1[0] = 3.2378;
    dat[1] = dat1[1] = 4.88;
    dat[2] = dat1[2] = 1.545;
    dat[3] = dat1[3] = 4.8057;
    dat[4] = dat1[4] = 1.6448;
    dat[5] = dat1[5] = 1.5957;
    dat[6] = dat1[6] = 8.1209;
*/
    //for(i=0; i<n; i++)
    //    cout << "油滴" << i << "的电量是：" << dat1[i] << endl;
    double th;  //Threshold 
    do{
        cout << "设定阈值(单位: 10^-19C): ";
        cin >> th;
        if(checkMin(dat, n, th)==0)
            cout << "阈值设置过小，电量数据均大于阈值！" << endl; 
    }while(checkMin(dat, n, th)==0);
    //cout << checkMin(dat, n, th) << endl;
    //cout << findMin(dat, n, th) << endl;
    double min;     //the value of min
    int m, m1;      //the sequence of min
    for(int j=0; j<nk; j++){
        cout << "---------------" << endl; 
        cout << "从第" << j+1 << "小的数据开始寻找最大公约数..." << endl;
        for(int i=0; i<n; i++)
            dat2[i] = dat1[i];
        m1 = findMin(dat1, n, th);
        while(checkMin(dat2, n, th)){
            m = findMin(dat2, n, th);
            min = dat2[m];
            //cout << "m=" << m << "   min=" << min << endl; 
            for(int i=0; i< n; i++){
                if(dat2[i] >= th)
                    dat2[i] -= (int)(dat2[i]/min) * min;
                //cout << "dat2[" << i << "]=" << dat2[i] << endl;
            }
            //cout << "-----------------" << endl;
        }
        //cout << "@@@m=" << m << "   min=" << min << endl; 
        cout << "本轮Euclid法求得最大公约数为：" << min << " *10^-19C" << endl;
        st[j] = min;
        dat1[m1] = 0;
    }
    double tot = 0;
    for(int j=0; j<nk; j++){
        //cout << st[j] << " " << endl;
        tot += st[j];
    }
    cout << "===========================" << endl;
    cout << "序号  电量(10^-19C)  电荷数" << endl;
    cout << "---------------------------" << endl; 
    for(int j=0; j<n; j++)
        cout << setw(3) << j+1 << setw(14) << setprecision(7) << dat[j] << setw(9) << int(dat[j]/(tot/nk)) << endl;
    cout << "===========================" << endl;
    cout << "e平均值为：" << tot/nk <<  " *10^-19C" << endl;
    system("pause");
    return 0;
}
