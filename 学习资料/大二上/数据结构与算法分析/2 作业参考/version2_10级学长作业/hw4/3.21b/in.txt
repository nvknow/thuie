char *filenameA = "Lena.JPG";
int main()
{

    IplImage *Src = cvLoadImage(filenameA);
 /* uchar4*  TemData_C =(unsigned char*)malloc( (Out->width)*(Out->height) );*/

    uchar4* ImaData_A =(uchar4*)malloc( (Src->width)*(Src->height)*sizeof(uchar4) );
    IplImage* Out = cvCreateImage(cvGetSize(Src),Src->depth, Src->nChannels );


	/*cudaMalloc((void**) &device_result, sizeof(char) * 11);
	  cudaMalloc((void**) &time, sizeof(clock_t));
	*/

  for(int i=0;i <Src->width;i++) 
    {
        for(int j = 0; j <Src->height; j++)
        { 
            ImaData_A[j*(Src->width)+i].x = ((unsigned char*)(Src->imageData+Src->widthStep*j))[i*3]; 
            ImaData_A[j*(Src->width)+i].y = ((unsigned char*)(Src->imageData+Src->widthStep*j))[i*3+1]; 
            ImaData_A[j*(Src->width)+i].z = ((unsigned char*))(Src->imageData+Src->widthStep*j))[i*3+2]; 
            ImaData_A[j*(Src->width)+i].w =  0; 
        }
    }
    uchar4* TemData_A = NULL;
    CUDA_SAFE_CALL( cudaMalloc( (void**)&TemData_A, ( Src->width*Src->height*sizeof(uchar4) ) ) );
    CUDA_SAFE_CALL( cudaMemcpy( TemData_A, ImaData_A, Out->width*Out->height*sizeof(uchar4), cudaMemcpyHostToDevice) );

    uchar4* TemData_C = NULL;
    CUDA_SAFE_CALL( cudaMalloc( (void**)&TemData_C, ( Src->width*Src->height*sizeof(uchar4) ) ) );
	   

    binarizeCall(TemData_A, TemData_C, Src->width, Src->height);

    uchar4* ImaData_C =(uchar4*)malloc( (Out->width)*(Out->height)*sizeof(uchar4) );
    CUDA_SAFE_CALL(cudaMemcpy( ImaData_C, TemData_C, Out->width*Out->height*sizeof(uchar4), cudaMemcpyDeviceToHost)); 

  for(int i=0;i <Out->width;i++) 
   {
        for(int j = 0; j <Out->height; j++)
        { 
            ((unsigned char*)(Out->imageData+Out->widthStep*j))[i*3]   = ImaData_C[j*Out->width+i].x; 
            ((unsigned char*)(Out->imageData+Out->widthStep*j))[i*3+1  = ImaData_C[j*Out->width+i].y; 
            ((unsigned char*)(Out->imageData+Out->widthStep*j))[i*3+2] = ImaData_C[j*Out->width+i].z;
        } 
    }
     cvNamedWindow("Res",CV_WINDOW_AUTOSIZE);
     cvShowImage("Res",Out);
     cvWaitKey(0);
     cvDestroyAllWindows();
     return 0;
  }
}
