//============================================================================
// Name        : checkBalance.cpp
// Author      : Donghui Li
// Version     :
// Copyright   : 
//============================================================================

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <stack>
#include <vector>

using namespace std;

struct node {
	string b;
	int line;
	int col;

	node(const string & x = "", int l = 0, int c = 0) :
			line(l), col(c) {
		b = x;
	}

	node(char x, int l, int c) :
			line(l), col(c) {
		b = x;
	}
};

bool flag = true;
void print(const node&n, const vector<string>& s, ostream & out) {
	flag = false;
	out << "Unbalanced \"" << n.b << "\"" << endl;
	out << "Line " << n.line + 1 << " : "
			<< s[n.line].substr(0, n.col + n.b.length()) << "<--";
	out << endl << endl;
}

void checkBracket(const vector<string>& s, char l, char r, ostream & out) {
	stack<node> b;
	for (size_t i = 0; i < s.size(); ++i) {
		const string &line = s[i];
		for (size_t j = 0; j < line.length(); ++j) {
			if (line[j] == l)
				b.push(node(l, i, j));
			else if (line[j] == r) {
				if (b.empty()){
					print(node(r, i, j), s, out);
					return;
				}
				else
					b.pop();
			}
		}
	}
	while (!b.empty()) {
		print(b.top(), s, out);
		b.pop();
	}
}

node findFirst(const vector<string>&s, string x, int l = 0, int c = 0) {
	size_t p;
	if ((p = s[l].find(x, c)) != string::npos) {
		return node(x, l, p);
	}
	for (size_t i = l + 1; i < s.size(); ++i) {
		if ((p = s[i].find(x)) != string::npos) {
			return node(x, i, p);
		}
	}
	return node("", 0, 0);
}

void checkComment(const vector<string>& s, ostream & out) {
	node l, r;
	r = node("", 0, -2);
	for (;;) {
		l = findFirst(s, "/*", r.line, r.col + 2);
		r = findFirst(s, "*/", r.line, r.col + 2);
		if (l.b == "" && r.b == "")
			break;
		if (l.b == "/*" && r.b != "*/") {
			print(l, s, out);
			return;
		}
		if (l.b != "/*" && r.b == "*/") {
			print(r, s, out);
			return;
		}
		if (l.line == r.line && l.col + 1 == r.col)
			r = findFirst(s, "*/", l.line, l.col + 2);
		if (l.line > r.line || (l.line == r.line && l.col + 2 > r.col)) {
			print(r, s, out);
			return;
		}
	};
}

int main() {
	ifstream fin("in.txt");
	ofstream fout("out.txt");
	vector<string> src;
	string line;

	getline(fin, line);

	while (!fin.fail()) {
		src.push_back(line);
		getline(fin, line);
	}

	checkBracket(src, '(', ')', fout);
	checkBracket(src, '[', ']', fout);
	checkBracket(src, '{', '}', fout);
	checkComment(src, fout);

	if (flag)
		fout << "All balanced!" << endl;

	return 0;
}
