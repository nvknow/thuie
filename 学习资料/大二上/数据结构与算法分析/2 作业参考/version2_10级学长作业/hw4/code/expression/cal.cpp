//============================================================================
// Name        : cal.cpp
// Author      : Donghui Li
// Version     :
// Copyright   : 
//============================================================================

//#define DEBUG

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stack>
#include <map>
#include <cctype>
#include <stdexcept>

using namespace std;

class expression {
public:
	expression(const string & s = string()) {
		//保存各个算符的优先级
		pr['+'] = 1;
		pr['-'] = 1;
		pr['*'] = 3;
		pr['/'] = 3;
		pr['('] = 5;

		setExp(s);
	}

	void setExp(const string &s) {
		exp.clear();
		for (size_t i = 0; i < s.length(); ++i)
			if (!isspace(s[i]))
				exp += s[i];
		addZero(exp);
		cerr << exp << endl;
	}

	//Deal with negative number like -10*(-10+3) by transform it to 0-10*(0-10+3)
	void addZero(string& s) {
		if (s[0] == '-' || s[0] == '+')
			s.insert(s.begin(), '0');

		size_t p = 0;
		while ((p = s.find('(', p)) != string::npos) {
			++p;
			if (s[p] == '-' || s[p] == '+')
				s.insert(s.begin() + p, '0');
		}
	}

	double cal() {
		if (exp.length()==1)
			throw runtime_error("Empty expression");
		istringstream in(exp);
		char c;
		double x;
		while (in) {
			if (isdigit(in.peek())) {
				in >> x;
#ifdef DEBUG
				cerr << x;
#endif
				num.push(x);
			} else {
				in >> c;
				if (in.fail())
					break;
				if (c == ';')
					break;
				if (c == ')') {
					while (!op.empty() && op.top() != '(') {
						popOp();
					}
					if (op.empty())
						throw runtime_error("Unbalanced ')'");
					op.pop();
				} else {
					if (pr.count(c) == 0)
						throw runtime_error(
								string("Illegal character '") + c + "'");
					while (!op.empty() && op.top() != '('
							&& pr[c] <= pr[op.top()]) {
						popOp();
					}
					op.push(c);
				}
			}
		}
		while (!op.empty())
			popOp();
		if (num.size() > 1)
			throw runtime_error(
					"Illegal expression, there may be not enough operators");
		return num.top();
	}
private:
	void popOp() {
		char c = op.top();
		op.pop();
		if (c == '(')
			throw runtime_error("Unbalanced '('");
#ifdef DEBUG
		cerr << c;
#endif
		double a, b;
		if (num.empty())
			throw runtime_error(
					"Illegal expression, there may be too many operators");
		b = num.top();
		num.pop();
		if (num.empty())
			throw runtime_error(
					"Illegal expression, there may be too many operators");
		a = num.top();
		num.pop();
		switch (c) {
		case '+':
			a += b;
			break;
		case '-':
			a -= b;
			break;
		case '*':
			a *= b;
			break;
		case '/':
			if (b == 0)
				throw runtime_error("Can not divide a number by 0");
			a /= b;
			break;
		}
		num.push(a);
	}
	stack<char> op;
	stack<double> num;
	map<char, int> pr;
	string exp;
};

int main() {
	ifstream fin("in.txt");
	ofstream fout("out.txt");
	while (fin) {
		string line;
		getline(fin, line);
		if (fin.fail())
			break;
		if (line[line.size() - 1] != ';')
			break;
		try {
			expression exp(line);
			fout << exp.cal() << endl;
		} catch (runtime_error & e) {
			fout << e.what() << endl;
			continue;
		}
	}

	return 0;
}
