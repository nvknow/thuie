/*
 * mergeSort.h
 *
 *  Created on: 2012-3-16
 *      Author: ldh
 */

#ifndef MERGESORT_H_
#define MERGESORT_H_

#include <vector>
using namespace std;

template<typename T>
void merge(vector<T> &v, vector<T> &t, int beg, int mid, int end) {
	int i = beg;
	int j = mid;
	int k = beg;

	while (i < mid && j < end)
		if (v[i] < v[j])
			t[k++] = v[i++];
		else
			t[k++] = v[j++];

	while (i < mid)
		t[k++] = v[i++];

	while (j < end)
		t[k++] = v[j++];

	for (i = beg; i < end; ++i)
		v[i] = t[i];
}

template<typename T>
void mergeSort(vector<T> &v, vector<T> &t, int beg, int end) {
	if (end - beg > 1) {
		int mid = (beg + end) / 2;
		mergeSort(v, t, beg, mid);
		mergeSort(v, t, mid, end);

		merge(v, t, beg, mid, end);
	}
}

template<typename T>
void mergeSort(vector<T> &v) {
	vector<T> t(v.size());

	mergeSort(v, t, 0, v.size());
}

#endif /* MERGESORT_H_ */
