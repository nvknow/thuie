#include <iostream>
#include <fstream>
#include <vector>

#include "dataGen.h"

using namespace std;

template<typename T>
ostream & operator<<(ostream & out, const vector<T> & v) {
	for (typename vector<T>::const_iterator i = v.begin(); i != v.end(); ++i)
		out << *i << ' ';

	return out;
}

template<typename T>
istream & operator>>(ostream & in, const vector<T> & v) {
	T x;
	in >> x;
	while (!in.fail()) {
		v.push_back(x);
		in >> x;
	}

	return in;
}

int main() {
	int n[]={20000, 40000,80000,160000,320000,640000};
	int count=sizeof(n)/sizeof(int);
	char fn[]="data_N0.txt";

	for(int i=0;i<count;++i){
		vector<int> v;
		dataGen(v,n[i],10000);

		++fn[6];
		ofstream fout(fn);

		fout << v <<endl;

		fout.close();
	}
}
