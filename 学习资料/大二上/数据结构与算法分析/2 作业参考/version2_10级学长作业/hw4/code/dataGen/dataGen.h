/*
 * dataGen.h
 *
 *  Created on: 2012-3-16
 *      Author: ldh
 */

#ifndef DATAGEN_H_
#define DATAGEN_H_

#include <cstdlib>
#include <ctime>
#include <vector>

using namespace std;

void dataGen(vector<int>& d, int n, int max=RAND_MAX) {
	/* initialize random seed: */
	srand(time(NULL)+clock());

	d.clear();
	d.reserve(n);

	for (int i = 0; i < n; ++i)
		d.push_back(rand()%max+1);
}

#endif /* DATAGEN_H_ */
