//============================================================================
// Name        : matrixOrder.cpp
// Author      : Donghui Li
//============================================================================

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

void printOrder(vector<vector<int> > & K, int l, int r, ostream & out) {
	if (l == r)
		out << char('A' - 1 + l);
	else {
		out << '(';
		printOrder(K, l, K[l][r], out);
		printOrder(K, K[l][r] + 1, r, out);
		out << ')';
	}
}

int main() {
	int n; //number of input
	cin >> n;

	vector<int> c(n); //input
	for (int i = 0; i < n; ++i)
		cin >> c[i];

	vector<vector<int> > f(n, vector<int>(n));
	vector<vector<int> > K(n, vector<int>(n));

	int i, j, k, min;
	for (j = 1; j < n; ++j) {
		f[j][j] = 0;
		for (i = j - 1; i > 0; --i) {
			min = -1;
			for (k = i; k < j; ++k)
				if (min == -1 || f[i][k] + f[k + 1][j] + c[i - 1] * c[k] * c[j] < min) {
					min = f[i][k] + f[k + 1][j] + c[i - 1] * c[k] * c[j];
					K[i][j] = k;
				}
			f[i][j] = min;
		}
	}

	ofstream fout("out.txt");
	printOrder(K, 1, n - 1, fout);
	fout << endl;
	fout << f[1][n-1] << endl;
	fout.close();

	return 0;
}
