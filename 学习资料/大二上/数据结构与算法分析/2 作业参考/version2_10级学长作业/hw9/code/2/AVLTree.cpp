//============================================================================
// Name        : AVLTree.cpp
// Author      : Donghui Li
//============================================================================

#include <iostream>
#include <fstream>
#include <string>

#include "AVLTree.h"

using namespace std;

int main( )
{
	ifstream fin1 ("4.9.in.txt");
	ofstream fout1("4.9.out.txt");
	ifstream fin2 ("4.19.in.txt");
	ofstream fout2("4.19.out.txt");

	AVLTree<int> t1,t2;

	t1.insertFromStream(fin1);
	t1.listAll(fout1);

	t2.insertFromStream(fin2);
	t2.listAll(fout2);

    return 0;
}
