/*
 * count1.cpp
 *
 *  Created on: 2012-3-9
 *      Author: ldh
 */

#include <iostream>
#include <fstream>

using namespace std;

int count(int n) {
	if (n > 0) {
		return n % 2 + count(n / 2);
	}
	return 0;
}

int main() {
	ifstream fin("in.txt");
	if (!fin) {
		cerr << "无法打开in.txt" << endl;
		return 1;
	}

	ofstream fout("out.txt");
	if (!fout) {
		cerr << "无法创建out.txt" << endl;
		return 2;
	}

	int n;

	fin >> n;

	while (!fin.fail()) {
		fout << count(n) << endl;
		fin >> n;
	}

	fout.close();

	return 0;
}
