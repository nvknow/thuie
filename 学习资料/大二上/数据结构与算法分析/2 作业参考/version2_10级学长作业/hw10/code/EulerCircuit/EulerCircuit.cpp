//============================================================================
// Name        : EulerCircuit.cpp
// Author      : 李冬辉
// Description : 判断输入的无向图是否存在欧拉回路，如果有则输出。
//============================================================================

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

//为了简化代码，使用了全局变量

vector<vector<int> > graph; //用邻接矩阵保存图
vector<int> indegree; //入度

int n, m; //顶点数和边数

//输入输出文件
ifstream fin("in.txt");
ofstream fout("out.txt");

//递归访问节点x
void walk(int x) {
	//寻找所有与x相连且没有访问过的边进行递归
	for (int i = 0; i < n; ++i)
		if (graph[x][i] > 0) {
			--graph[x][i];
			--graph[i][x];
			walk(i);
		}

	//在这个位置输出就相当于后序输出
	fout << x + 1 << ' ';
}

int main() {
	fin >> n >> m;

	//设置vector大小并初始化
	graph.resize(n, vector<int>(n, 0));
	indegree.resize(n, 0);

	//读入图并统计入度
	int i, u, v;
	for (i = 0; i < m; ++i) {
		fin >> u >> v;
		--u;
		--v; //从0开始
		++graph[u][v];
		++graph[v][u];
		++indegree[u];
		++indegree[v];
	}

	//通过奇点个数判断是否存在欧拉回路
	int odd = 0; //奇点个数
	for (i = 0; i < n; ++i)
		if (indegree[i] & 1)//利用位运算判断是否为偶数
			++odd;
	if (odd > 0) {
		fout << "Euler circuit does not exist!" << endl;
		return -1;
	}

	//从任意点开始遍历，这里取0
	walk(0);
	fout<<endl;

	return 0;
}
