//============================================================================
// Name        : jobGreedy.cpp
// Author      : Donghui Li
//============================================================================

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

struct Job {
	int no, due, profit;
};

bool comp(Job*a, Job*b) {
	return a->profit > b->profit;
}

int main() {
	fstream fin("in.txt");
	vector<Job*> job;
	int dueMax = 0;

	while (fin) {
		Job *t = new Job;
		t->no = job.size() + 1;
		fin >> t->due >> t->profit;
		if (fin.fail()) {
			delete t;
			break;
		}
		if (t->due > dueMax)
			dueMax = t->due;
		job.push_back(t);
	}

	sort(job.begin(), job.end(), comp);

	int i, j;
	int sum = 0;

	vector<Job*> arr(dueMax + 1, 0);

	for (i = 0; i < job.size(); ++i) {
		for (j = job[i]->due; j > 0; --j) {//为了处理方便，将时间轴向右平移了一个单位
			if (arr[j] == 0) {
				arr[j] = job[i];
				sum += job[i]->profit;
				break;
			}
		}
	}

	ofstream fout("out.txt");
	fout << sum << endl;

	for (i = 0; i < dueMax + 1; ++i) {
		if (arr[i])
			fout << arr[i]->no << ' ';
	}
	fout << endl;
	fout.close();

	return 0;
}
