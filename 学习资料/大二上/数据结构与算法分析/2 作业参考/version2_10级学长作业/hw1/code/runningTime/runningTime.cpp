#include <iostream>
#include <ctime>
#include <vector>

using namespace std;

//Run program fragment no with value n
int run(int n, int no) {
	static clock_t start;
	int sum, i, j, k, time;

	start = clock();

	switch (no) {
	case 1:
		sum = 0;
		for (i = 0; i < n; i++)
			sum++;
		break;

	case 2:
		sum = 0;
		for (i = 0; i < n; i++)
			for (j = 0; j < n; j++)
				sum++;
		break;

	case 3:
		sum = 0;
		for (i = 0; i < n; i++)
			for (j = 0; j < n * n; j++)
				sum++;
		break;

	case 4:
		sum = 0;
		for (i = 0; i < n; i++)
			for (j = 0; j < i; j++)
				sum++;
		break;

	case 5:
		sum = 0;
		for (i = 0; i < n; i++)
			for (j = 0; j < i * i; j++)
				for (k = 0; k < j; k++)
					sum++;
		break;

	case 6:
		sum = 0;
		for (i = 1; i < n; i++)
			for (j = 1; j < i * i; j++)
				if (j % i == 0)
					for (k = 0; k < j; k++)
						sum++;
		break;
	}

	time = clock() - start;

	cout << n << ", " << time << ", " << sum << "," << endl;

	return time;
}

int main() {
	const int count = 3;

	int data[6][count] = {
		{ 20000000, 40000000, 80000000 },
		{ 4000, 8000, 16000 }, 
		{ 200, 400, 800 }, 
		{ 4000, 8000, 16000 }, 
		{ 40, 60, 80 }, 
		{ 80, 160, 320 }
	};

	int no, i;

	//Run something to make sure the program is stable.
	{
		int temp = 0;
		for (i = 0; i < 200000000; i++)
			temp++;
	}

	for (no = 0; no < 6; no++) {
		cout << "Program fragment " << no + 1 << endl;
		cout << "N , T , sum," << endl;
		for (i = 0; i < count; i++)
			run(data[no][i], no + 1);
	}

	cout << "Fin" << endl;

	return 0;
}