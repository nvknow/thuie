//#define DEBUG

#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

int run(char *ifn, char *ofn){
	ifstream fin;
	ofstream fout;

	fin.open(ifn);
	if(!fin){
		cerr << "Unable to open file: " <<ifn<<endl;
		return 1;
	}

	char c = fin.get();
	int wordCount = 0, alphaCount = 0;
	double ave=0;
	bool inWord = false;

	while (fin.good()) // loop while reading is possible
	{
		//assume all input is legal, this program can not deal with illegal character
		if (c < 'A') {
			inWord = false;
		} else {
			if (!inWord) {
				wordCount++;
				inWord = true;
			}
			alphaCount++;
		}
		c = fin.get();
	}

	if(wordCount == 0){
		cerr << "No word found!!" << endl;
		return 2;
	}

	ave = 1.0 * alphaCount / wordCount;

#ifdef DEBUG
	cout << "wc:" << wordCount << " aC:" << alphaCount << " ave:"
			<< ave << endl;
#endif

	fout.open(ofn);
	fout<<setprecision(3)<<ave<<endl;
	fout.close();

	return 0;
}

int main() {
	run("in_1.txt", "out_1.txt");
	run("in_2.txt", "out_2.txt");

	return 0;
}